//import pages
import { AboutUs } from "./pages/AboutUs";
//Global style
import GlobalStyle from "./components/GlobalStyle";
import { Nav } from "./components/Nav";
import { ContactUs } from './pages/ContactUs';
import { OurWork } from "./pages/OurWork";
import { MovieDetail } from "./pages/MovieDetail";
//Router
import { Route, Switch, useLocation } from "react-router-dom";
//Animation
import { AnimatePresence } from "framer-motion"; 
// permite que los elementos se animen cuando se eliminan del arbol de react (cuando cambias de pagina)
// exitBeforeEnter espera a que la animacion de salida termina para elminar el arbol

function App() {
  const location = useLocation();
  return (
    <div className="App">
      <GlobalStyle />
      <Nav />
      <AnimatePresence 
        exitBeforeEnter 
        onExitComplete={() => {
          window.scrollTo(0,0);
        }}   
      >
        <Switch location={location} key={location.pathname}>
          <Route path="/" exact component={ AboutUs} />
          <Route path="/work" component={OurWork} exact/>
          <Route path="/work/:id" component={MovieDetail} exact/>
          <Route path="/contact" component={ContactUs} exact/>
        </Switch>
      </AnimatePresence>
    </div>
  );
}

export default App;
